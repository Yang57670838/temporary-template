import React, { useEffect, useState } from 'react'
import Dashboard from './components/pages/Dashboard'
import { LanguageStore } from './contexts/LanguageContext'
import { UserInfoStore } from './contexts/UserInfoContext'
import WindowsContext from './contexts/WindowsContext'
import './App.scss'

const App: React.FC = () => {

  const [isMobile, setIsMobile] = useState<boolean>(window.innerWidth < 500)
  const [isSmallScreen, setIsSmallScreen] = useState<boolean>(window.innerWidth < 1015)

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => window.removeEventListener('resize', handleWindowSizeChange)
  }, [])

  const handleWindowSizeChange = () => {
    console.log('current window size', window.innerWidth)
    setIsMobile(window.innerWidth < 500)
    setIsSmallScreen(window.innerWidth < 1015)
  }

  return (
    <div className="App">
      <WindowsContext.Provider value={{isMobile, isSmallScreen}}>
        <UserInfoStore>
          <LanguageStore>
            <Dashboard />
          </LanguageStore>
        </UserInfoStore>
      </WindowsContext.Provider>
    </div>
  );
}

export default App