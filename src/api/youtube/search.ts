import baseRequest from './base'

const mockQuery = 'building'

const searchRequest = async () => {
    return baseRequest.get('/search', {
        params: {
            // q: mockQuery
            channelId: 'UC3Ml4pLTWrb4LItUymId4Lg'
        }
    })
}
export default searchRequest