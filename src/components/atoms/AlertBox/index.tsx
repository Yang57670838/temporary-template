// for alert message under fields in the form
import React, { useState } from 'react'
import cx from 'classnames'

import './index.scss'

export interface Props {
    message: string;
    type: 'warning' | 'infor';
}

const AlertBox: React.FC<Props> = ({ message, type }) => {


    return (
        <>
            <div className={cx('alert-box', {'warning-box': type==='warning'}, {'infor-box': type==='infor'})} >
                <div className={cx('icon-section', {'warning-icon': type==='warning'}, {'infor-icon': type==='infor'})}>
                    <img src={'information.svg'} width='24px' height='24px' className="icon" />
                </div>
                <div className="text">
                    {message}
                </div>
            </div>
        </>
    )
}

export default AlertBox
