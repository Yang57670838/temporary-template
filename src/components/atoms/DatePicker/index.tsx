import React, { useState } from 'react'
import DatePicker from "react-datepicker"

import "react-datepicker/dist/react-datepicker.css"

import './index.scss'

export interface Props {
    later?: boolean;
    label: string;
    id: string;
}

const DatePick: React.FC<Props> = ({ later = false, label, id }) => {
    const [startDate, setStartDate] = useState<Date | null>(null)

    const CustomInput = ({ value, onClick }: any) => (
        <input
            className="datepicker-input"
            onClick={onClick}
            value={value}
            aria-labelledby={`date-picker-${id}`} 
        />
    )

    return (
        <div>
            <span className="label" id={`date-picker-${id}`}>{label}</span>
            <DatePicker
                dateFormat="dd/MM/yyyy"
                selected={startDate}
                minDate={later ? new Date() : null}
                onChange={date => setStartDate(date)}
                customInput={<CustomInput />}
            />
        </div>
    )
}

export default DatePick
