import React from 'react';
import Dropzone from 'react-dropzone';

import './index.scss'

export class FileUpload extends React.PureComponent {

    render() {
        const onDrop = (acceptedFiles: any) => {
            console.log(acceptedFiles);
        }
        return (
            <div className="drop-container">
                <div className="drop-message">
                    <div className="upload-icon"></div>
                    Drag & Drop files here or click to upload
</div>
                <Dropzone
                    onDrop={onDrop}
                    accept="image/png, image/gif, application/xml, application/pdf"
                    minSize={0}
                    maxSize={5242880} //5MB
                >
                    {({ getRootProps, getInputProps, isDragActive, isDragReject }) => {
                        return (
                            <div {...getRootProps()}>
                                <input {...getInputProps()} />
                                {isDragActive ? "Drop it like it's hot!" : 'Click me or drag a file to upload!'}
                                {isDragReject && "File type not accepted, sorry!"}
                            </div>
                        )
                    }}
                </Dropzone>
            </div>
        );
    }
}

export default FileUpload;
