// render  one image inside image list in the grid only.. 
import React, { useState, Fragment } from 'react'

interface Props {
    imageDetails: {
        description: string;
        urls: {
            regular: string;
        }
    };
}

class ImageCell extends React.Component<Props, {}> {

    state = { spans: 0 }
    private imageRef = React.createRef<HTMLImageElement>()

    componentDidMount() {
        if (this.imageRef.current) {
            this.imageRef.current.addEventListener('load', this.setSpans)
        }
    }

    setSpans = () => {
        if (this.imageRef.current) {
            const height = this.imageRef.current.clientHeight
            const spans = Math.ceil(height / 10)
            this.setState({ spans })
        }
    }

    render() {
        const { description, urls } = this.props.imageDetails
        return (
            <div style={{ gridRowEnd: `span ${this.state.spans}` }}>
                <img alt={description} src={urls.regular} ref={this.imageRef} />
            </div>
        )
    }
}

export default ImageCell