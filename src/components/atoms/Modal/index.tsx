import React from 'react'
import ReactModal from 'react-modal'


import './index.scss'

export interface LoadingProps {
    loading: boolean;
}

const Loading: React.FC<LoadingProps> = ({ loading = true }) => {
    return (
        <>
            {
                loading && (
                    <div className="spinner">
                        <FadeLoader height={15} width={5} radius={2} margin="2" color={'#2aa4a2'} loading={loading} />
                    </div>
                )
            }
        </>
    )
};

export default Loading
