import React from 'react'
import { connect } from 'react-redux'
import { ThunkDispatch } from 'redux-thunk'
import { AnyAction } from 'redux'
import { EventType } from '../../../types'
import { uploadSingleFile } from '../../../store/actions/file'

interface DispatchProps {
    uploadSingleFileAction: (eventType: EventType, file: File) => void;
}

export interface SimpleFileUploadProps {
    multiple?: boolean;
    accept: string; // accept files format, separate by commas: "application/pdf, .jpg, .xls, .xlsx, text/plain, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
}

type AllProps = DispatchProps & SimpleFileUploadProps

const SimpleFileUpload: React.FC<AllProps> = ({accept, multiple = false, uploadSingleFileAction}) => {

    const uploadHandler = (e: any)  => {
        const file: File = (e.target.files as FileList)[0] // select first file, for now, use allow one file input..
        uploadSingleFileAction('XMLMock', file)
    }

    return (
        <>
            <input type="file" name="testfile"  accept={accept} multiple={multiple}  onChange={uploadHandler}/>
        </>
    )
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>): DispatchProps => {
    return {
        uploadSingleFileAction: (eventType, file) => dispatch(uploadSingleFile(eventType, file))
    }
  };
  
  export default connect(
    undefined,
    mapDispatchToProps,
  )(SimpleFileUpload);
  
