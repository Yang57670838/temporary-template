import { useState, useEffect } from 'react'
import axios from 'axios'
import { ProductProfile, EndpointResponse } from '../../types'

const useGetProfiles = () => {
    const [profiles, setProfiles] = useState<ProductProfile[]>([])
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [isError, setIsError] = useState<boolean>(false)
  
    const fetchProfiles = async () => {
        setIsError(false)
        setIsLoading(true)
        try {
            //to do: put end points in another place
            const response: EndpointResponse<ProductProfile> = await axios.get('https://api.jsonbin.io/b/5cae9a54fb42337645ebcad3')
            console.log('response', response)
            setProfiles(response.data)
        } catch (error) {
            setIsError(true)
        }
        setIsLoading(false)
    }
  
    useEffect(() => {
        fetchProfiles()
    }, [])

    return {
        profiles,
        isLoading,
        isError
    }
}

export default useGetProfiles