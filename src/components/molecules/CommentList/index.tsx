import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { values } from "lodash"
import { commentAction } from '../../../store/actions'
import { Store, Comment } from '../../../types';

interface DispatchProps {
    fetchComments: () => void;
}

interface StateProps  {
    isLoading: boolean;
    comments: Comment[];
}

interface OwnProps {}

type CommentListProps = DispatchProps & StateProps & OwnProps

const CommentList: React.FC<CommentListProps> = ({fetchComments}) => {

    useEffect(() => {
        fetchComments()
    }, [])

    return (
      <div>
          comments list here...
      </div>
    );
}

// const mapStateToProps = (state: Store): StateProps => {
// 	return {
//         isLoading: state.comments.isLoading,
// 		comments: values(state.comments.commentsById)
// 	}
// }

export default connect<{}, DispatchProps, OwnProps>(undefined, {
    fetchComments: commentAction.fetchComments
})(CommentList)
