import React, { useState, useEffect } from 'react'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

// for example: showing list of customers or a list of transactions informations in a table..
const GridTable: React.FC = () => {
    const [columnDefs, changeColumnDefs] = useState<any>([{
        headerName: "Name", field: "fullName", sortable: true, filter: true, checkboxSelection: true
    }, {
        headerName: "Company", field: "company", sortable: true, filter: true
    }, {
        headerName: "Email", field: "email", sortable: true, filter: true
    },
    {
        headerName: "Contact", field: "primaryNumber", filter: true
    },
    {
        headerName: "Amount", field: "amount", sortable: true, filter: true
    },
    {
        headerName: "Date", field: "date", sortable: true, filter: true
    }])

    const [rowData, changeRowData] = useState<any>([])
    const [gridApi, setGridApi] = useState<any>()

    useEffect(() => {
        // to do: fetch later
        const result = [{
            id: '1', company: "Toyota", fullName: "Celica", amount: 35000, email: '1@a.com', primaryNumber: '12345', date: '02-Feb-2017'
        }, {
            id: '2', company: "Ford", fullName: "Mondeo", amount: 32000, email: '1@a.com', primaryNumber: '12345', date: '02-Mar-2015'
        }, {
            id: '3', company: "Porsche", fullName: "Boxter", amount: 72000, email: '1@a.com', primaryNumber: '12345', date: '15-Dec-2017'
        }]
        changeRowData(result)
    }, [])// to do: change dependencies if need to update table data later


    const submit = () => {
        const selectedNodes = gridApi.getSelectedNodes()
        const selectedData = selectedNodes.map( node => node.data )
        console.log(selectedData[0].company)
    }

    return (
        <div
            className="ag-theme-balham"
            style={{
                height: '500px',
                width: '1000px'
            }}
        >
            <AgGridReact
                columnDefs={columnDefs}
                rowData={rowData}
                rowSelection="multiple"
                onGridReady={params => setGridApi(params.api)}
            />
            <button onClick={submit}> Submit </button>
        </div>
    );
}

export default GridTable;
