import React from 'react'
import LanguageContext from '../../../contexts/LanguageContext'

import './header.scss'

//change to functional component with context hooks later
class Header extends React.Component {

  static contextType = LanguageContext

  render() {
    return (
      <header>
        <div className="logo">My Logo</div>
        <div className="headerRight">
          {/* to do: change to language select later */}
          <button onClick={() => this.context.changeLanguage('chinese-simplified')}>简体中文</button>
          <button onClick={() => this.context.changeLanguage('english')}>english</button>
          <input />
        </div>
      </header>
    );
  }
}

export default Header