import React, { Fragment, createRef } from 'react'
import { connect } from 'react-redux'
import $ from "jquery"
import { Service } from '../../../types/services'
import LanguageContext from '../../../contexts/LanguageContext'
import UserInfoContext from '../../../contexts/UserInfoContext'
import ProfileCard from '../../atoms/ProfileCard'
import { profileAction } from '../../../store/actions'

(window as any).jQuery = $
(window as any).$ = $

require("jquery-ui-sortable")
require("formBuilder")

const formData = [
  {
    type: "header",
    subtype: "h1",
    label: "formBuilder in React"
  },
  {
    type: "paragraph",
    label: "This is a demonstration of formBuilder running in a React project."
  }
]


const mockProducts: Service[] = []

class ProfileCardList extends React.Component {

    fb = createRef()
    componentDidMount() {
        $(this.fb.current).formBuilder({ formData });
    }

    render() {
        // fetch from end point now for list of profiles
        return (
            <div>
                {/* <UserInfoContext.Consumer>
                    {(userInfo) => userInfo && (
                        <div>
                            username: {userInfo.username}
                            <LanguageContext.Consumer>
                                {({language}) => (
                                    <div>
                                        language: {language}
                                    </div>
                                )}
                            </LanguageContext.Consumer>
                        </div>
                    )
                    }
                </UserInfoContext.Consumer> */}

                <div id="fb-editor" ref={this.fb} />    


                {mockProducts.map((product: Service, index: number) => (
                    <Fragment key={index}>
                        <ProfileCard />
                    </Fragment>
                ))}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        profiles: state.profiles
    }
}

export default connect(mapStateToProps, {
    requestProfiles: profileAction.requestProfiles
})(ProfileCardList)