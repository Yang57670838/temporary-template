import React from 'react'
import ReactTooltip from 'react-tooltip'
import ImageCell from '../../atoms/ImageCell'
import { debounce } from '../../../utils/common'
import Loading from '../../atoms/Loading'

import './index.scss'

interface Props {
    images: Array<any>; //to do: images from mock api, so change fixed types later 
}

const ProfileImagesList: React.FC<Props> = ({ images }) => {

    console.log('test here images result', images)
    const mockFetch = () => {
        console.log('calling mockFetch')
    }
    const debouncedMockFetch = debounce(mockFetch, 3000)

    const imageRender = images.map(i => {
        return <ImageCell imageDetails={i} key={i.id} />
    })

    return (
        <>
            <div className="image-list">{imageRender}</div>
            <input
                type="text"
                onChange={debouncedMockFetch}
                placeholder="Search for a place"
            />
            <div data-tip data-for="contentTips">
                <img src={'information.svg'} width='24px' height='20px' />
            </div>
            <ReactTooltip id='contentTips' place='right' type='light' effect='solid' border>
                hello
            </ReactTooltip>
            <Loading loading={false} />
        </>
    )
}

export default ProfileImagesList