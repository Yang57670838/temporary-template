import React from 'react'
import { useDrop } from 'react-dnd'
import cx from 'classnames'
import DatePick from '../../../atoms/DatePicker'
import AlertBox from '../../../atoms/AlertBox'

import './index.scss'

const Board: React.FC = () => {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'field',
    drop: () => ({ name: 'Board' }),
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  })

  const isActive = canDrop && isOver

  return (
    <div ref={drop} className={cx('board', {"activeBoard": isActive})}>
      <div>
        <input />
        <DatePick later={true} label="Date (dd/mm/yyyy)" id="pay-time" />
        <AlertBox message="test here" type="warning" />
        <AlertBox message="test here" type="infor" />
      </div>
    </div>
  )
}

export default Board