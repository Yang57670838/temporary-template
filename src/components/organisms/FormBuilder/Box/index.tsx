import React from 'react'
import { useDrag, DragSourceMonitor } from 'react-dnd'
import cx from 'classnames'

import './index.scss'

interface BoxProps {
  name: string;
}

const Box: React.FC<BoxProps> = ({ name }) => {
  const [{ isDragging }, drag] = useDrag({
    item: { name, type: 'field'},
    end: (item: { name: string } | undefined, monitor: DragSourceMonitor) => {
      const dropResult = monitor.getDropResult()
      if (item && dropResult) {
        alert(`You dropped ${item.name} into ${dropResult.name}!`)
      }
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  })

  return (
    <div ref={drag} className={cx('box', {'draggingBox': isDragging, 'noDraggingBox': !isDragging})} >
      {name}
    </div>
  )
}

export default Box
