import React from 'react'
import Board from './Board'
import Box from './Box'

import './index.scss'

const FormBuilder: React.FC = () => {

    return (
        <div className="formBuilder">
            <Board />
            <div className="formPanel">
                <Box name="Input" />
                <Box name="Select" />
            </div>
        </div>
    )
}


export default FormBuilder