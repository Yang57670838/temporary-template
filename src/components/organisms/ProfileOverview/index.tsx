import React, { useState } from 'react'
import ProfileCardList from '../../molecules/ProfileCardList'
import useGetProfiles from '../../hooks/useGetProfiles'
import LanguageContext from '../../../contexts/LanguageContext'

const ProfileOverview: React.FC = () => {
    const [selectedSize, changeSelectedSize] = useState<string | null>()
    const {profiles, isLoading, isError} = useGetProfiles()

    const hook = React.useContext(LanguageContext)

    let componentContent
    switch(hook && hook.language) {
        case 'english':
            componentContent = {
                button1:'Big Size',
                button2:'Small Size'
            }
            break;
        case 'chinese-simplified':
            componentContent = {
                button1: '大号',
                button2: '小号'
            }
            break;
        default:
            componentContent = {
                button1: 'Big Size',
                button2: 'Small Size'
            }
    }

    
    return (
    <div className="App">
        <button onClick={() => changeSelectedSize('large')}>{componentContent.button1}</button>
        <button onClick={() => changeSelectedSize('small')}>{componentContent.button2}</button>
        {isError && <div>Something went wrong ...</div>}
        {isLoading ? (
            // to do: replace to loading bar later
            <div>Loading...</div>
        ) : (
            <ProfileCardList />
        )}
    </div>
  );
}

export default ProfileOverview;
