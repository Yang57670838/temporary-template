import React, { useEffect, useState } from 'react'
import cx from 'classnames'
import { Video } from '../../../../types/youtube'
import WindowsContext from '../../../../contexts/WindowsContext'

import './index.scss'

export interface Props {
    video: Video;
    onVideoSelect: (video: Video) => void;
}

const VideoItem: React.FC<Props> = ({ video, onVideoSelect }) => {

    const windowsHook = React.useContext(WindowsContext)

    return (
        <>
            {
                windowsHook && (
                    <div
                        className={cx(
                            'video-item',
                            { 'video-item-big': !windowsHook.isSmallScreen },
                            { 'video-item-small': windowsHook.isSmallScreen }
                        )}
                        onClick={() => onVideoSelect(video)}
                    >
                        <img src={video.snippet.thumbnails.medium.url} alt={video.snippet.title} />
                        <div className="video-item-title">{video.snippet.title}</div>
                    </div>
                )
            }
        </>
    )
}


export default VideoItem