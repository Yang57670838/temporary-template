import React, { useEffect, useState } from 'react'
import cx from 'classnames'
import { Video } from '../../../../types/youtube'
import VideoItem from '../VideoItem'
import WindowsContext from '../../../../contexts/WindowsContext'

import './index.scss'

export interface Props {
    videos: Array<Video>;
    onVideoSelect: (video: Video) => void;
}

const VideoList: React.FC<Props> = ({ videos, onVideoSelect }) => {

    const windowsHook = React.useContext(WindowsContext)

    const renderList = videos.map((v, i) => {
        return <VideoItem key={i} video={v} onVideoSelect={onVideoSelect} />
    })

    return (
        <>
            {
                windowsHook && (
                    <div 
                    className={cx(
                        'video-list',
                        {'video-list-big': !windowsHook.isSmallScreen},
                        {'video-list-small': windowsHook.isSmallScreen}
                    )}>
                        {renderList}
                    </div>
                )
            }
        </>
    )
}


export default VideoList