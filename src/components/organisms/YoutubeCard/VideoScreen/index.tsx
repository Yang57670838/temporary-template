import React, { useEffect, useState } from 'react'
import cx from 'classnames'
import { Video } from '../../../../types/youtube'
import WindowsContext from '../../../../contexts/WindowsContext'

import './index.scss'

export interface Props {
    video: Video | null;
}

const VideoScreen: React.FC<Props> = ( { video }) => {

    const windowsHook = React.useContext(WindowsContext)

    return (
        <>
            {windowsHook && video && (
                <div className={cx(
                    'video-screen',
                    {'video-screen-small': windowsHook.isSmallScreen}
                )}>
                    <div className="ratio-16x9">
                        <iframe 
                            src={`http://www.youtube.com/embed/${video.id.videoId}`} 
                            title="youtube video player"
                            allowFullScreen
                        />
                    </div>
                    <h4>{video.snippet.title}</h4>
                    <p>{video.snippet.description}</p>
                </div>
            )}
        </>
    )
}


export default VideoScreen