import React, { useEffect, useState } from 'react'
import cx from 'classnames'
import youtubeSearch from '../../../api/youtube/search'
import { Video } from '../../../types/youtube'
import VideoList from './VideoList'
import VideoScreen from './VideoScreen'
import WindowsContext from '../../../contexts/WindowsContext'

import './index.scss'

const YoutubeCard: React.FC = () => {
    const [videos, setVideos] = useState<Array<Video>>([])
    const [selectedVideo, setSelectedVideo] = useState<Video | null>(null)
    const windowsHook = React.useContext(WindowsContext)

    useEffect(() => {
        const request = async () => {
            const res = await youtubeSearch()
            setVideos(res.data.items)
            setSelectedVideo(res.data.items[0])
        }
        request()
    }, [])

    const onVideoSelect = (video: Video): void => {
        setSelectedVideo(video)
    }

    return (
        <>
            {
                windowsHook && (
                    <div className={cx(
                        {'youtube-card-big': !windowsHook.isSmallScreen},
                        {'youtube-card-small': windowsHook.isSmallScreen}
                    )}>
                        <VideoScreen video={selectedVideo} />
                        <VideoList videos={videos} onVideoSelect={onVideoSelect} />
                    </div>
                )
            }
        </>
    )
}


export default YoutubeCard