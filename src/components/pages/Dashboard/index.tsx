import React, { Fragment, useState} from 'react'
import axios from 'axios'
import ProfileOverview from '../../organisms/ProfileOverview'
import Header from '../../molecules/Header'
import CommentList from '../../molecules/CommentList'
import GridTable from '../../molecules/GridTable'
import ProfileImagesList from '../../molecules/ProfileImagesList'
import FormBuilder from '../../organisms/FormBuilder'
import YoutubeCard from '../../organisms/YoutubeCard'
import FileUpload from '../../atoms/FileUpload'
import SimpleFileUpload from '../../atoms/SimpleFileUpload'

const Dashboard: React.FC = () => {

  // test for get mock photos for profile images
  const [images, setImages] = useState<Array<any>>([])
  const buttonHandler = async () => {
    const response = await axios.get('https://api.unsplash.com/search/photos', {
      params: { query: 'dating' },
      headers: {
        Authorization: 'Client-ID 74e567fdf8cc4efc5e90d4fc6c9bbd2f98586d41da2fec3728fb99018e80ceba'
      }
    })
    setImages(response.data.results)
  }

  return (
    <Fragment>
        <Header />
        <ProfileOverview />
        <CommentList />
        <GridTable />
        <br />
        <br />
        <br />
        <br />
        <br />
        <button onClick={buttonHandler}>get mock profile images</button>
        <ProfileImagesList images={images} />

        <br />
        <FormBuilder />
        <br />
        <br />
        <br />
        <br />
        <br />
        <YoutubeCard />


        <FileUpload />

        <SimpleFileUpload accept="application/pdf, .jpg, .xls, .xlsx, text/plain, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, .xml" />

    </Fragment>
  );
}

export default Dashboard;
