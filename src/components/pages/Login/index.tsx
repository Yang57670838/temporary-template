import React, { Fragment, useState, useEffect } from 'react'
import { Store, LoginStore } from '../../../types'
import { connect } from 'react-redux'
import {ThunkDispatch } from 'redux-thunk'
import { AnyAction } from 'redux'
import { RouteComponentProps } from 'react-router'

interface StateProps {
  isLogin: boolean;
  token: string;
}

interface DispatchProps {}

interface OwnProps extends RouteComponentProps {}

type AllProps = StateProps & DispatchProps & OwnProps;

const Login: React.FC<AllProps> = (props) => {
  useEffect(() => {
    if (props.token) {
      props.history.push('/')
    }
  }, [props.isLogin])

  return (
    <Fragment>
       login atoms here later
    </Fragment>
  );
}

const mapStateToProps: (state: Store) => StateProps = state => ({
  isLogin: state.login && state.login.isLoading, //check login loading status in hooks as this way, to redirect after success login, can change other ways later
  token: state.login && state.login.token
});

export default connect(
  mapStateToProps,
  null,
)(Login);
