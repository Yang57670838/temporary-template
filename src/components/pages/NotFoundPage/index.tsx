import React, { Fragment} from 'react'

const NotFoundPage: React.FC = () => {

  return (
    <Fragment>
       <h1> Page not Found </h1>
       <p> This page does not exist</p>
    </Fragment>
  );
}

export default NotFoundPage