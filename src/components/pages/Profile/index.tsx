import React, { Fragment} from 'react'
import { RouteComponentProps } from 'react-router';


interface OwnProps extends RouteComponentProps<{ id: string }> {}

interface StateProps {}

export type ComponentProps = StateProps & OwnProps;

const Profile: React.FC<ComponentProps> = props => {


  return (
    <Fragment>
       profile page here for user id: {props.match.params.id}
    </Fragment>
  );
}

export default Profile
