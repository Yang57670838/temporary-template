import React from 'react';
import { connect } from 'react-redux';
import { RouteProps } from 'react-router';
import { Store, LoginStore } from '../../../types';
import { Route } from 'react-router-dom';
import Login from '../Login'

interface StoreProps {
    account: LoginStore;
}
export interface ComponentProps extends StoreProps, RouteProps { }

export const ProtectedRoute: React.FC<ComponentProps> = (props: ComponentProps) => {
    const { component: Component, account, ...rest } = props;
    const now = Date.now() / 1000
    const isAccountValid = account && account.expires_at && (account.expires_at - now > 0)
    return <Route {...rest} component={isAccountValid ? Component : Login} />;
};

const mapStateToProps = (state: Store) => ({
    account: state.login
});

export default connect(
    mapStateToProps,
    null,
)(ProtectedRoute);
