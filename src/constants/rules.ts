// define a set of rules for each login roles or users or groups, 
// related to field editable or not, eatch button enable or not, piece of page show or hide...etc...

//there are four levels of contents, restricted > confidential > internal > public
export type Levels = 'restricted' | 'confidential' | 'internal' | 'public';

export type Roles = 'admin' | 'staff' | 'customer';

export const rules = {
    restricted: {
        admin: false,
        staff: false,
        customer: false
    },
    confidential: {
        admin: true,
        staff: false,
        customer: false
    },
    internal: {
        admin: true,
        staff: true,
        customer: false
    },
    public: {
        admin: true,
        staff: true,
        customer: true
    }
}