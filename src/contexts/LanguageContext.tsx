import React, { useState } from 'react'

export interface LanguageContextInterface {
  language: string;
  changeLanguage: Function
}

const Context = React.createContext<LanguageContextInterface | null>(null)

export const LanguageStore: React.FC = ({ children }) => {
    const [language, changeLanguage] = useState<string>('english')
  
    return (
      <Context.Provider value={{language, changeLanguage}}>
        {children}
      </Context.Provider>
    )
}

export default Context
