import React, { useState } from 'react'
import { UserInfo } from '../types/user'

export interface UserInfoContextInterface {
    userInfo: UserInfo;
    changeUserInfo: Function
}

const Context = React.createContext<UserInfoContextInterface | null>(null)
  
export const UserInfoStore: React.FC = ({ children }) => {
    const [userInfo, changeUserInfo] = useState<UserInfo>({
        username: 'Test Name',
        isAuthenticated: true,
        userID: '12345'
    }) // to do: change to login infor from api
    
    return (
        <Context.Provider value={{userInfo, changeUserInfo}}>
          {children}
        </Context.Provider>
    )
}
  
export default Context
