import React, { useState } from 'react'

export interface WindowsContextInterface {
    isSmallScreen: boolean;
    isMobile: boolean;
}

export default React.createContext<WindowsContextInterface | null>(null)