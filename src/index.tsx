import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import { DndProvider } from 'react-dnd'
import Backend from 'react-dnd-html5-backend'
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom'
import thunk from 'redux-thunk'
import './styles/styles.scss';
import App from './App'
import Login from './components/pages/Login'
import Contact from './components/pages/ContactUs'
import NotFoundPage from './components/pages/NotFoundPage'
import Profile from './components/pages/Profile'
import ProtectedRoute from './components/pages/ProtectedRoute'
import reducers from './store/reducers'
import * as serviceWorker from './serviceWorker'



//to do: add 'process.env.NODE_ENV': JSON.stringify('production') in Webpack config for the production. 
// to do: set no-any to false in your tslint.json file
const composeEnhancers = process.env.NODE_ENV !== 'production' && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const middlewares = [thunk]
if (process.env.NODE_ENV !== 'production') {
    const { createLogger } = require('redux-logger')
    const logger = createLogger({ duration: true })
    middlewares.push(logger)
}

const store = createStore(reducers, composeEnhancers(applyMiddleware(...middlewares)))

const Main = () => (
    <BrowserRouter>
        <Link to='/login'>login</Link>
        <Switch>
             {/* to do: remove login check for dashboard during dev until login works.. */}
            {/* <ProtectedRoute exact path='/' component={App} /> */}
            <Route exact path='/' component={App} />
            <Route exact path='/login' component={Login} />
            <ProtectedRoute exact path='/contact' component={Contact} />
            <ProtectedRoute exact path='/profile/:id' component={Profile} />
            <Route component={NotFoundPage} /> 
        </Switch>
    </BrowserRouter>
)

ReactDOM.render(
    <Provider store={store}>
        <DndProvider backend={Backend} >
            <Main />
        </DndProvider>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
