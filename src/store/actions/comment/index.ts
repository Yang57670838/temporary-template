import axios from 'axios'
import { actions as ActionTypes } from '../../../constants/action'
import { Comment } from '../../../types'
import { JSON_PLACE_HOLDER_BASE_URL } from '../../../constants/config'


export const fetchComments = () => {

    return async (dispatch) => {
        dispatch(fetchCommentsStarted())
        try {
            const response = await axios.get(`${JSON_PLACE_HOLDER_BASE_URL}/comments`)
            dispatch(fetchCommentsSuccess(response.data))
        } catch (error) {
            // to do: add error message or error code later
            dispatch(fetchCommentsFailed())
        }
    }
} 

export const fetchCommentsStarted = () => {
    return {
        type: ActionTypes.FETCH_COMMENTS_STARTED
    }
}

export const fetchCommentsSuccess = (comments: Comment[]) => {
    return {
        type: ActionTypes.FETCH_COMMENTS_SUCCESS,
        comments
    }
}

// to do: add error into payload with its type later
export const fetchCommentsFailed = () => {
    return {
        type: ActionTypes.FETCH_COMMENTS_FAILED,
    }
}
