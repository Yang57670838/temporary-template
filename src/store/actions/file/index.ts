import { actions as ActionTypes } from '../../../constants/action'
import { EventType } from '../../../types'
import {
    UploadSingleFileStartedAction,
    UploadSingleFileSuccessAction,
    UploadSingleFileFailedAction
} from '../../../types/file'
import axios from 'axios'
import { ActionCreator, Dispatch } from 'redux'
import { ThunkAction } from 'redux-thunk'

export const uploadSingleFile: ActionCreator<ThunkAction<
    Promise<UploadSingleFileSuccessAction | UploadSingleFileFailedAction>,
    string,
    null,
    UploadSingleFileSuccessAction | UploadSingleFileFailedAction
>> = (eventType: EventType, file: File) => { 

    return async (dispatch: Dispatch) => {
        dispatch(uploadSingleFileStarted())
        try {
            const data = new FormData()
            data.append('uploadfile', file)
            const response = await axios.post(`http://localhost:8000/upload`, data, {
                params: { eventType }
            })
            return dispatch(uploadSingleFileSuccess(response.data))
        } catch (error) {
            return dispatch(uploadSingleFileFailed())
        }
    }
} 

// this action can be used to upload different types of files, xml, pdf, or jpg.... then res will be different
// will depends on purpose, end point will perforce differently, but all in one action 'UPLOAD_SINGLE_FILE_STARTED'
// single file one time only for now..
// Return: server will return JSON string or null.. depends on upload purpose
export const uploadSingleFileStarted: () => UploadSingleFileStartedAction = () => {
    return {
        type: ActionTypes.UPLOAD_SINGLE_FILE_STARTED
    }
}

// string is JSON string in argument
export const uploadSingleFileSuccess: (resData: string | null) => UploadSingleFileSuccessAction = (resData) => {
    return {
        type: ActionTypes.UPLOAD_SINGLE_FILE_SUCCESS,
        resData
    }
}

export const uploadSingleFileFailed: () => UploadSingleFileFailedAction = () => {
    return {
        type: ActionTypes.UPLOAD_SINGLE_FILE_FAILED,
    }
}
