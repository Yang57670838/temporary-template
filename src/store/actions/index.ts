import * as profileAction from './profile'
import * as commentAction from './comment'
import * as fileAction from './file'

export { profileAction, commentAction, fileAction }
