import { actions as ActionTypes } from '../../../constants/action'
import { Account } from '../../../types'

// to do: add action types later
export const loginStarted = () => async dispatch => {
    // fetch to end point here, to do:..
    return {
        type: ActionTypes.LOGIN_STARTED
    }
}

export const loginSuccess = (account: Account) => {
    return {
        type: ActionTypes.LOGIN_SUCCESS,
        account
    }
}

// to do: add error details into payload with its type later
export const loginFailed = () => {
    return {
        type: ActionTypes.LOGIN_FAILED,
    }
}
