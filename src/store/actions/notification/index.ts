import { actions as ActionTypes } from '../../../constants/action'
import {
    NotificationUpdateAction,
    NoticeType
} from '../../../types/notification'

export const updateNotification: (isOpend: boolean, content?: string, noticeType?: NoticeType) => NotificationUpdateAction = (isOpend, content, noticeType) => {
    return {
        type: ActionTypes.UPDATE_NOTIFICATION,
        isOpend,
        content,
        noticeType
    }
}