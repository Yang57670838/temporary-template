import { actions as ActionTypes } from '../../../constants/action'
import { Service } from '../../../types'

// to do: add action types later
export const requestProfiles = () => {
    return {
        type: ActionTypes.REQUEST_SERVICES
    }
}

export const requestProfilesSuccess = (services: Service[]) => {
    return {
        type: ActionTypes.REQUEST_SERVICES_SUCCESS,
        services
    }
}

// to do: add error into payload with its type later
export const requestProfilesFailed = () => {
    return {
        type: ActionTypes.REQUEST_SERVICES_FAILED,
    }
}
