import axios from 'axios'
import { actions as ActionTypes } from '../../../constants/action'
import { UserDetails } from '../../../types'
import { JSON_PLACE_HOLDER_BASE_URL } from '../../../constants/config'

export const fetchUser = (id) => async dispatch => {
            dispatch(fetchCommentsStarted())
        try {
            const response = await axios.get(`${JSON_PLACE_HOLDER_BASE_URL}/users/${id}`)
            dispatch(fetchCommentsSuccess(response.data))
        } catch (error) {
            dispatch(fetchCommentsFailed())
        }
} 

export const fetchUserStarted = () => {
    return {
        type: ActionTypes.FETCH_USER_STARTED
    }
}

export const fetchUserSuccess = (user: UserDetails) => {
    return {
        type: ActionTypes.FETCH_USER_SUCCESS,
        user
    }
}

// to do: add error into payload with its type later
export const fetchUserFailed = () => {
    return {
        type: ActionTypes.FETCH_USER_FAILED,
    }
}
