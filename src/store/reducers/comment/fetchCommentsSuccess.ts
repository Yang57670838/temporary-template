import { mapKeys } from 'lodash'
import { CommentStore, Comment } from '../../../types';

function fetchCommentsSuccess(state: CommentStore, comments: Comment[]): CommentStore {
  return {
    ...state,
    isLoading: false,
    commentsById: mapKeys(comments, ({id}) => id)
  }
}

export default fetchCommentsSuccess
