import { Reducer } from 'redux'
import { actions as ActionTypes } from '../../../constants/action'
import fetchCommentsSuccessHandler from './fetchCommentsSuccess'
import { CommentStore, CommentsAction } from '../../../types/comments'

const initialState = {
    error: false,
    isLoading: false,
    commentsById: null
};

export const commentReducer: Reducer<CommentStore, CommentsAction> = (state: CommentStore = initialState, action: CommentsAction): CommentStore => {
    switch (action.type) {
        case ActionTypes.FETCH_COMMENTS_STARTED:
          return {
            ...state,
            error: false,
            isLoading: true
          };
        case ActionTypes.FETCH_COMMENTS_SUCCESS:
          return fetchCommentsSuccessHandler(state, action.comments)
        case ActionTypes.FETCH_COMMENTS_FAILED:
          return {
            ...state,
            error: true, // to do: change to error object later
            isLoading: false
          };
        default:
          return state;
      }
}