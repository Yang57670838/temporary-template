import { Reducer } from 'redux'
import { actions as ActionTypes } from '../../../constants/action'
import { FileStore, FileAction } from '../../../types/file'

const initialState = {
    error: false,
    isUploading: false,
    resData: null
};

export const fileReducer: Reducer<FileStore, FileAction> = (state: FileStore = initialState, action: FileAction): FileStore => {
    switch (action.type) {
        case ActionTypes.UPLOAD_SINGLE_FILE_STARTED:
          return {
            ...state,
            error: false,
            isUploading: true
          };
        case ActionTypes.UPLOAD_SINGLE_FILE_SUCCESS:
            return {
                ...state,
                error: false,
                isUploading: false,
                resData: action.resData
              };
        case ActionTypes.UPLOAD_SINGLE_FILE_FAILED:
          return {
            ...state,
            error: true, // to do: change to error object later
            isUploading: false
          };
        default:
          return state;
      }
}