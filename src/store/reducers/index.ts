import { serviceReducer } from './service'
import { commentReducer } from './comment'
import { fileReducer } from './file'
import { notificationReducer } from './notification'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    services: serviceReducer,
    comments: commentReducer,
    file: fileReducer,
    notification: notificationReducer
})

export default rootReducer

export type AppState = ReturnType<typeof rootReducer>

