import { Reducer } from 'redux'
import { actions as ActionTypes } from '../../../constants/action'
import { LoginStore, LoginAction } from '../../../types/login'

// to do: add types to reducer later
const initialState = {
    error: false,
    isLoading: false,
    id: null,
    fullName: null
};

export const loginReducer: Reducer<LoginStore, LoginAction> = (state: LoginStore = initialState, action: LoginAction): LoginStore => {
    switch (action.type) {
        case ActionTypes.LOGIN_STARTED:
          return {
            ...state,
            error: false,
            isLoading: true
          };
        case ActionTypes.LOGIN_SUCCESS:
          return {
            ...state,
            id: action.account.id,
            fullName: action.account.fullName,
            error: false,
            isLoading: false
          };
        case ActionTypes.LOGIN_FAILED:
          return {
            ...state,
            error: true, // to do: change to error object later or null
            isLoading: false
          };
        default:
          return state;
      }
}