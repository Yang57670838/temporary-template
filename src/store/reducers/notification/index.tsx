import { Reducer } from 'redux'
import { actions as ActionTypes } from '../../../constants/action'
import { NotificationStore, NotificationAction } from '../../../types/notification'

const initialState = {
    noticeType: undefined,
    content: undefined,
    isOpend: false
};

export const notificationReducer: Reducer<NotificationStore, NotificationAction> = (state: NotificationStore = initialState, action: NotificationAction): NotificationStore => {
    switch (action.type) {
        case ActionTypes.UPDATE_NOTIFICATION:
          return {
            ...state,
            isOpend: action.isOpend,
            content: action.content,
            noticeType: action.noticeType
          };
        default:
          return state;
      }
}