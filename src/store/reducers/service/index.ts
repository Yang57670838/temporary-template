import { Reducer } from 'redux'
import { actions as ActionTypes } from '../../../constants/action'
import { ServiceStore, ServicesAction } from '../../../types/services'

// to do: add types to reducer later
const initialState = {
    error: false,
    isLoading: false,
    servicesById: null
};

export const serviceReducer: Reducer<ServiceStore, ServicesAction> = (state: ServiceStore = initialState, action: ServicesAction): ServiceStore => {
    switch (action.type) {
        case ActionTypes.REQUEST_SERVICES:
          return {
            ...state,
            error: false,
            isLoading: true
          };
        case ActionTypes.REQUEST_SERVICES_SUCCESS:
          return {
            ...state,
            servicesById: null, // to do: fix later
            error: false,
            isLoading: false
          };
        case ActionTypes.REQUEST_SERVICES_FAILED:
          return {
            ...state,
            error: true, // to do: change to error object later or null
            isLoading: false
          };
        default:
          return state;
      }
}