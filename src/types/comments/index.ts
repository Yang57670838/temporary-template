import { actions as ActionTypes } from '../../constants/action'

export interface Comment {
    id: string;
    name: string;
    email: string;
    body: string;
}

export interface FetchCommentsStartedAction {
  type: ActionTypes.FETCH_COMMENTS_STARTED;
}

export interface FetchCommentsSuccessAction {
  type: ActionTypes.FETCH_COMMENTS_SUCCESS;
  comments: Comment[];
}

export interface FetchCommentsFailedAction {
  type: ActionTypes.FETCH_COMMENTS_FAILED;
}

export type CommentsAction =
| FetchCommentsStartedAction
| FetchCommentsSuccessAction
| FetchCommentsFailedAction;

export interface CommentStore {
    commentsById: { [id: string]: Comment } | null;
    isLoading: boolean;
    error: boolean;
}