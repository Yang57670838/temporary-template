export interface EndpointResponse<T> {
    data: T[];
    status: number;
}