import { actions as ActionTypes } from '../../constants/action'

// types to tell end point how to deal with the uploaded file, and how to send back res
export type EventType = 'XMLMock' | 'PdfReader';

export interface UploadSingleFileStartedAction {
  type: ActionTypes.UPLOAD_SINGLE_FILE_STARTED;
}

export interface UploadSingleFileSuccessAction {
  type: ActionTypes.UPLOAD_SINGLE_FILE_SUCCESS;
  resData: string | null;
}

export interface UploadSingleFileFailedAction {
  type: ActionTypes.UPLOAD_SINGLE_FILE_FAILED;
}

export type FileAction =
| UploadSingleFileStartedAction
| UploadSingleFileSuccessAction
| UploadSingleFileFailedAction;

export interface FileStore {
    resData: string | null;
    isUploading: boolean;
    error: boolean;
}