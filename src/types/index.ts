import { Service, ServiceStore } from './services'
import { EndpointResponse } from './common'
import { Comment, CommentStore } from './comments'
import { UserDetails, UserStore } from './user'
import { LoginStore, Account } from './login'
import { EventType, FileStore } from './file'
import { NotificationStore, NoticeType } from './notification'

interface Store {
    services: ServiceStore;
    comments: CommentStore;
    users: UserStore;
    login: LoginStore;
    file: FileStore;
    notification: NotificationStore;
}

export {
    Service,
    ServiceStore,
    EndpointResponse,
    Comment,
    CommentStore,
    Store,
    UserDetails,
    UserStore,
    LoginStore,
    Account,
    EventType,
    FileStore,
    NotificationStore,
    NoticeType
}