import { actions as ActionTypes } from '../../constants/action'

export interface Account {
    id: number;
    fullName: string;
}

export interface LoginStartedAction {
  type: ActionTypes.LOGIN_STARTED;

}

export interface LoginSuccessAction {
  type: ActionTypes.LOGIN_SUCCESS;
  account: Account;
}

export interface LoginFailedAction {
  type: ActionTypes.LOGIN_FAILED;
}

export type LoginAction =
| LoginStartedAction
| LoginSuccessAction
| LoginFailedAction;

export interface LoginStore {
  id: number | null;
  fullName: string | null;
  token: string;
  expires_at: number; // seconds from January 1, 1970 00:00:00 UTC.
  isLoading: boolean;
  error: boolean;
}