import { actions as ActionTypes } from '../../constants/action'

export type NoticeType = "warning" | "error" | "infor";

export interface NotificationUpdateAction {
  type: ActionTypes.UPDATE_NOTIFICATION;
  isOpend: boolean;
  content?: string;
  noticeType?: NoticeType;
}

export type NotificationAction =
| NotificationUpdateAction;

export interface NotificationStore {
  noticeType?: NoticeType;
  content?: string;
  isOpend: boolean;
}