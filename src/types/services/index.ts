import { actions as ActionTypes } from '../../constants/action'

export interface Service {
    index: number;
    isSale: boolean;
    isExclusive: boolean;
    price: string;
    serviceImage: string;
    serviceName: string;
}

export interface RequestServicesAction {
  type: ActionTypes.REQUEST_SERVICES;

}

export interface RequestServicesSuccessAction {
  type: ActionTypes.REQUEST_SERVICES_SUCCESS;
  services: Service[];
}

export interface RequestServicesFailedAction {
  type: ActionTypes.REQUEST_SERVICES_FAILED;
}

export type ServicesAction =
| RequestServicesAction
| RequestServicesSuccessAction
| RequestServicesFailedAction;

export interface ServiceStore {
  servicesById: { [id: string]: Service } | null;
  isLoading: boolean;
  error: boolean;
}