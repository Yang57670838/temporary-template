import { actions as ActionTypes } from '../../constants/action'

// log in user infor
export interface UserInfo {
    username: string;
    isAuthenticated: boolean;
    userID: string;
    role: 
}

// full details of a user
export interface UserDetails {
    id: string
    name: string; // full name
    email: string;
    address: object; //to do: change to fixed structure
    phone: string;
    companyId?: string;
}

export interface FetchUserStartedAction {
    type: ActionTypes.FETCH_USER_STARTED;
  }
  
  export interface FetchUserSuccessAction {
    type: ActionTypes.FETCH_USER_SUCCESS;
    user: UserDetails;
  }
  
  export interface FetchUserFailedAction {
    type: ActionTypes.FETCH_COMMENTS_FAILED;
  }
  
  export type UserAction =
  | FetchUserStartedAction
  | FetchUserSuccessAction
  | FetchUserFailedAction;
  
  export interface UserStore {
      usersById: { [id: string]: UserDetails } | null;
      isLoading: boolean;
      error: boolean;
  }