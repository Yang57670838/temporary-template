// missing contents, only put useful keys below
export interface Video {
    id: {
        videoId: string;
    };
    snippet: {
        publishedAt: string;
        channelId: string;
        title: string;
        description: string;
        thumbnails: {
            default: Thumbnail;
            medium: Thumbnail;
            high: Thumbnail;
        },
        channelTitle: string;
    }
}

interface Thumbnail {
    url: string;
    width: number;
    height: number;
}