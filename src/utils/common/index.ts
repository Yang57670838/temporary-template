const debounce = (fn: any, delay: number): any => {
    let timer: any = null
    return function (this: any, ...args: any) {
        const context = this
        timer && clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(context, args);
        }, delay)
    }
}

export{ debounce } 