import { rules, Roles, Levels } from "../../constants/rules";

// check with rules based on content levels and user roles, then return permission true or false
const check = (role: Roles, contentLevel: Levels) : boolean => {
  const permissions = rules[contentLevel][role]
  return permissions
}